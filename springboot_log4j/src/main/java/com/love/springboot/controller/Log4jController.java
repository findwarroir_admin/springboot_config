package com.love.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author aishiyushijiepingxing
 * @description
 * @date 2020/5/18
 */
@RestController
public class Log4jController {
    private static final Logger LOGGER = LoggerFactory.getLogger(Log4jController.class);

    @RequestMapping("/printLog")
    public String printLog() {
        //共有8个级别，按照从低到高为：ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < OFF
        LOGGER.error("error 日志级别");
        LOGGER.warn("warn 日志级别");
        LOGGER.info("info 日志级别");
        LOGGER.debug("debug 日志级别");
        LOGGER.trace("trace 日志级别");
        return "printLog";
    }
}
