package com.love.springboot;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@NacosPropertySource(dataId = "springboot-log4j", autoRefreshed = true)
@RestController
public class SpringBootLog4j {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootLog4j.class, args);
    }

}