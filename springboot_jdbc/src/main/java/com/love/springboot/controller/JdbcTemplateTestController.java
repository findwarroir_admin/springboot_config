package com.love.springboot.controller;

import com.love.springboot.Mapper.MyRowMapper;
import com.love.springboot.config.JdbcTemplateObject;
import com.love.springboot.entity.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author aishiyushijiepingxing
 * @description
 * @date 2020/6/4
 */
@RequestMapping("/jdbcTemplateTest")
public class JdbcTemplateTestController {

    @RequestMapping("/testJdbcTemplate")
    public void testJdbcTemplate() {
        JdbcTemplateObject jdbcTemplateObject = new JdbcTemplateObject();
        JdbcTemplate jdbcTemplate = jdbcTemplateObject.getJdbcTemplate();
        String sql = "select * from dpsdk where channel_id = ?";
        String channelId = "1";
        User user = jdbcTemplate.queryForObject(sql, new MyRowMapper(), channelId);
        System.out.println(user.toString());
        jdbcTemplate.update("sql");
    }


}
