package com.love.springboot.Mapper;

import com.love.springboot.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 实现RowMapper接口，返回Dpsdk对象
 */
public class MyRowMapper implements RowMapper<User> {

    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        //获取结果集中的数据
        String channelId = resultSet.getString("channel_id");
        String channelName = resultSet.getString("channel_name");
        String location = resultSet.getString("location");
        String pid = resultSet.getString("pid");
        User user = new User();
        user.setChannelId(channelId);
        user.setChannelName(channelName);
        user.setPid(pid);
        user.setLocation(location);
        return user;
    }
}
